# Bletchley Contributing Guide

The goal of this document is to create a contribution process that:

* Encourages new contributions.
* Encourages contributors to remain involved.
* Avoids unnecessary processes and bureaucracy whenever possible.
* Creates a transparent decision making process which makes it clear how
contributors can be involved in decision making.

This document is based on much prior art in the Node.js community, io.js,
and the Node.js project.

## Vocabulary

* A **Contributor** is any individual creating or commenting on an issue or pull request (PR).
* A **Committer** is a subset of contributors who have been given write access to the repository.

# Logging Issues

Log an issue for any question or problem you might have. When in doubt, log an issue, 
any additional policies about what to include will be provided in the responses. The only
exception is security disclosures which should be sent privately.

Committers may direct you to another repository, ask for additional clarifications, and
add appropriate metadata before the issue is addressed.

Please be courteous, respectful, and every participant is expected to follow the 
project's Code of Conduct.

# Contributions

Any change to resources in this repository must be through pull requests. This applies to all changes
to documentation, code, binary files, etc. Even long term committers must use
pull requests.

No pull request can be merged without being reviewed.

For non-trivial contributions, pull requests should sit for at least 48 hours to ensure that
contributors in other timezones have time to review. Consideration should also be given to 
weekends and other holiday periods to ensure active committers all have reasonable time to 
become involved in the discussion and review process if they wish.

The default for each contribution is that it is accepted once no committer has an objection.
During review committers may also request that a specific contributor who is most versed in a 
particular area gives a "Looks Good To Me" before the PR can be merged. There is no additional "sign off" 
process for contributions to land. Once all issues brought by committers are addressed it can 
be landed by any committer.

In the case of an objection being raised in a pull request by another committer, all involved 
committers should seek to arrive at a consensus by way of addressing concerns being expressed 
by discussion, compromise on the proposed change, or withdrawal of the proposed change.

If a contribution is controversial and committers cannot agree about how to get it to land
or if it should land then it should be put to a simple vote. Atleast 60% of comitters must agree
to the decision. If a tie is reached, the contribution should be escalated to the maintainer. 
As the maintainer is ultimately reposonible for the on-going development of Bletchley, they are given
the ability to decide tie-breaks within the community.

# Becoming a Committer

All contributors who land a non-trivial contribution should be on-boarded in a timely manner,
and added as a committer, and be given write access to the repository.

Committers are expected to follow this policy and continue to send pull requests, go through
proper review, and have other committers merge their pull requests.

